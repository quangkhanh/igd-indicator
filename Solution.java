import java.util.Random;
import java.lang.Math;
import java.util.ArrayList;

public class Solution implements Cloneable {
    final int N_GEN = 20;
    final int N_OBJ = 2;

    double[] gene = new double[N_GEN];
    double[] object = new double[N_OBJ];
    double fitness;
    double[] distance = new double[100000];
    int rank ; 

    Random rand = new Random();

    public Solution cloneme() {
        Solution s = new Solution();
        for (int i = 0; i < N_GEN; i++)
            s.gene[i] = this.gene[i];
        for (int i = 0; i < N_OBJ; i++)
            s.object[i] = this.object[i];
        return s;
    }

    Solution() {
        for (int i = 0; i < N_GEN; i++) {
            gene[i] = rand.nextDouble();
        }
    }

    void getObject() {
        double sum = 0;
        for (int i = 1; i < N_GEN; i++)
            sum += gene[i];
        sum = 1 + 9 * sum / (N_GEN - 1);
        object[0] = gene[0];
        object[1] = sum * (1 - Math.sqrt(gene[0] / sum));
    }
}