import java.util.ArrayList ;

public class Reference {
    final int MAX_SIZE = 30000;
    final int N_GEN = 20;
    final int N_OBJ = 2;
    final int p = 40;

    double[][] refset = new double[MAX_SIZE][N_OBJ];
    ArrayList<Solution> refpoints = new ArrayList<Solution>() ; 
    int[] a = new int[N_OBJ];
    int s;
    int size;

    Reference() {
        s = 0;
        size = 0;
    }

    public void build(int i) {
        if (i == N_OBJ - 1) {
            a[i] = p - s;
            for (int j = 0; j < N_OBJ; j++) {
                refset[size][j] = (double) a[j] * 1 / p;
            }
            size++;
        } else
            for (int j = 0; j <= p - s; j++) {
                a[i] = j;
                s += j;
                build(i + 1);
                s -= a[i];
            }
    }
    // public void rebuild()
    // {
    // for(int i = 0 ; i < size ; i++)
    // {
    // for(int j = 0 ; j < N_GEN ; j++)
    // {
    // refset[i][j] *= (double)1/p ;
    // }
    // }
    // }
}