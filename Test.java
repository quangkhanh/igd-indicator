import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Currency;
import java.util.Random;
import java.io.PrintWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Prerun {
    final int N_GEN = 20;
    final int N_OBJ = 2;
    final int p = 40;
    final int Iteration = 10000;
    final double Mu_rate = 0.3;
    final int POP_Size = 100;

    Population population = new Population();
    ArrayList<Solution> referset = new ArrayList<Solution>();

    public static void main(String[] args) throws CloneNotSupportedException {
        Prerun run = new Prerun();
        run.population.ref.build(0);
        for (run.population.current = 0; run.population.current < run.population.N_OBJ; run.population.current++) {
            int counter = 0;
            run.population.initialize();
            while (counter < run.Iteration) {
                run.population.setGenePool();
                run.population.evolution();
                run.population.populationSort();
                counter++;
            }
            System.out.println(run.population.population.get(0).object[1]);
            System.out.println(run.population.population.get(0).fitness);

            run.population.extremepoint[run.population.current] = (Solution) run.population.population.get(0).clone();
            System.out.println(run.population.extremepoint[run.population.current].object[0] + "  "
                    + run.population.extremepoint[run.population.current].object[1]);
            for (int i = run.population.population.size() - 1; i >= 0; i--)
                run.population.population.remove(i);
            System.out.println("do dai: " + run.population.population.size());
            System.out.println("lan thu" + run.population.current);
        }
        try {
        FileWriter fileWriter = new FileWriter("data.csv");
        PrintWriter printWriter = new PrintWriter("data.csv");
        for (int i = 0; i < run.referset.size(); i++)
        printWriter.println(run.referset.get(i).object[0] + "," +
        run.referset.get(i).object[1]);
        printWriter.close();
        } catch (Exception e) {
        e.getMessage();
        }
    }
}
