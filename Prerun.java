import java.util.Comparator;
import java.util.Currency;
import java.util.Random;
import java.io.PrintWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Prerun {
    static final int N_GEN = 20;
    static final int N_OBJ = 2;
    static final int p = 40;
    static final int Iteration = 10;
    static final double Mu_rate = 0.3;
    static final int POP_Size = 100;

    Population population = new Population();
    ArrayList<Solution> referset = new ArrayList<Solution>();

    public static void main(String[] args) throws CloneNotSupportedException {
        Prerun run = new Prerun();
        run.population.ref.build(0);
        for (run.population.current = 0; run.population.current < 2*run.population.N_OBJ; run.population.current++) {
            int counter = 0;
            run.population.initialize();
            while (counter < run.Iteration) {
                run.population.setGenePool();
                run.population.evolution();
                run.population.populationSort();
                counter++;
                System.out.println(counter);
            }
            if(run.population.current >= run.N_OBJ){
                run.population.idealpoint.object[run.population.current - run.N_OBJ] = 
                run.population.population.get(0).object[run.population.current - run.N_OBJ] ; 
                continue ; 
            }
            run.population.nadirpoint.object[run.population.current] = 
            run.population.population.get(0).object[run.population.current] ; 
            run.population.extremepoint[run.population.current] = (Solution) run.population.population.get(0).cloneme();
            for (int i = run.population.population.size() - 1; i >= 0; i--)
                run.population.population.remove(i);
            System.out.println(counter + "  " + run.population.current) ; 
        }
        for(int i = 0 ; i < run.population.ref.size ; i++){
            Solution s = new Solution() ; 
            for(int j = 0 ; j < N_OBJ ; j++){
                s.object[j] = 
                run.population.ref.refset[i][j] * (run.population.nadirpoint.object[j] - run.population.idealpoint.object[j]) + 
                run.population.idealpoint.object[j] ; 
            }
            run.population.ref.refpoints.add(s);
            System.out.println(i) ; 
        }
        // for(int counter = 0 ; counter < run.Iteration ; counter++)
        // {
        //     run.population.initialize();
        // }

        try {
        FileWriter fileWriter = new FileWriter("data.csv");
        PrintWriter printWriter = new PrintWriter("data.csv");
        for (int i = 0; i < run.population.ref.refpoints.size(); i++) 
        printWriter.println(run.population.ref.refpoints.get(i).object[0] + "," +
        run.population.ref.refpoints.get(i).object[1] );
        printWriter.close();
        } catch (Exception e) {
        e.getMessage();
        }
    }
}