import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

public class Population {
    final int N_GEN = 20;
    final int N_OBJ = 2;
    final int Iteration = 1000;
    final double Mu_rate = 0.3;
    final int POP_Size = 100;

    ArrayList<Solution> population = new ArrayList<Solution>();
    ArrayList<Solution> genepool = new ArrayList<Solution>();
    Solution[] extremepoint = new Solution[N_OBJ];
    Solution nadirpoint = new Solution() ; 
    Solution idealpoint = new Solution() ; 
    int[] rankquantity = new int[3] ; 

    Reference ref = new Reference();
    int current = 0;

    void evaluate(Solution s) {
        s.getObject();
        s.fitness = 0;

        if(current < N_OBJ)
        {
            for(int i = 0 ; i < N_OBJ ; i++){
                s.fitness += 1000 * s.object[i] * s.object[i] ; 
            }
            s.fitness = s.fitness - 1000 * s.object[current] * s.object[current] + s.object[current] ; 
        }
        else{
            s.fitness = s.object[current - N_OBJ] ; 
        }
    }

    Random rand = new Random();

    int check(Solution s1 , Solution s2){
        int s = 0 ; 
        for(int i = 0 ; i < N_OBJ ; i++){
            if(s1.object[i] < s2.object[i]) s++ ; 
        }
        if(s == N_OBJ) return 1 ; 
        else if(s == 0) return -1 ;
        else return 0 ; 
    }

    void setRank()
    {
        int  x2 = 0 ; 
        for(int i = 0 ; i < 3 ; i++) rankquantity[i] = 0 ; 
        for(int i = 0 ; i < population.size() ; i++){
            for(int j = 0 ; j < ref.size ; j++){
                if(check(population.get(i), ref.refpoints.get(j)) == 1){
                    population.get(i).rank = 0 ;  
                    rankquantity[0]++ ; 
                    break ; 
                }
                else if(check(population.get(i), ref.refpoints.get(j)) == 0) x2++ ;  
            }
            if(x2 == ref.size){rankquantity[1]++ ;  population.get(i).rank = 1;} 
            else{rankquantity[2]++ ; population.get(i).rank = 2;} 
        }
    }

    void getDistance()
    {
        for(int i = 0 ; i < population.size() ; i++){
            for(int j = 0 ; j < ref.refpoints.size() ; j++){
                if(population.get(i).rank == 0) 
                {
                    double sum = 0 ; 
                    for(int k = 0 ; k < N_OBJ ; j++)
                    sum += (population.get(i).object[k] - ref.refpoints.get(j).object[k])*
                    (population.get(i).object[k] - ref.refpoints.get(j).object[k]) ; 
                    population.get(i).distance[j] = -Math.sqrt(sum) ; 
                }
                else 
                if(population.get(i).rank == 1){
                    double sum = 0 ; 
                    for(int k = 0 ; k < N_OBJ ; k++)
                    sum += (Math.max(population.get(i).object[k] - ref.refpoints.get(j).object[k], 0)*
                    Math.max(population.get(i).object[k] - ref.refpoints.get(j).object[k], 0));
                }
                else{
                    double sum = 0 ; 
                    for(int k = 0 ; k < N_OBJ ; j++)
                    sum += (population.get(i).object[k] - ref.refpoints.get(j).object[k])*
                    (population.get(i).object[k] - ref.refpoints.get(j).object[k]) ; 
                    population.get(i).distance[j] = Math.sqrt(sum) ;
                }
            }
        }
    }

    // void select()
    // {
        
    // }

    void initialize() {
        for (int i = 0; i < POP_Size; i++) {
            population.add(new Solution());
            evaluate(population.get(i));
        }
    }

    void setGenePool() {
        for (int k = 0; k < 2 * POP_Size; k++) {
            int i = rand.nextInt(POP_Size);
            int j = rand.nextInt(POP_Size);
            if (population.get(i).fitness < population.get(j).fitness)
                genepool.add(population.get(i));
            else
                genepool.add(population.get(j));
        }
    }

    Solution crossOver(Solution s1, Solution s2) throws CloneNotSupportedException {
        Solution so1, so2;
        so1 = (Solution) s1.cloneme();
        so2 = (Solution) s2.cloneme();
        int i = rand.nextInt(N_GEN);
        for (int j = 0; j <= i; j++) {
            double temp = s1.gene[j];
            so1.gene[j] = s2.gene[j];
            so2.gene[j] = temp;
        }
        mutate(so1);
        evaluate(so1);
        evaluate(so2);
        if (so1.fitness < so2.fitness)
            return so1;
        else
            return so2;
    }

    Solution mutate(Solution s) {
        for (int i = 0; i < N_GEN; i++) {
            double j = rand.nextDouble();
            if (j <= Mu_rate) {
                s.gene[i] = rand.nextDouble();
            }
        }
        return s;
    }

    void evolution() throws CloneNotSupportedException {
        setGenePool();
        while (genepool.size() > 1) {
            int i = rand.nextInt(genepool.size());
            Solution s1 = (Solution) genepool.get(i).cloneme();
            genepool.remove(i);
            i = rand.nextInt(genepool.size());
            Solution s2 = (Solution) genepool.get(i).cloneme();
            genepool.remove(i);
            population.add(crossOver(s1, s2));
        }
    }

    class SolutionComparator implements Comparator<Solution> {
        public int compare(Solution s1, Solution s2) {
            return Double.compare(s1.fitness, s2.fitness);
        }
    };

    void populationSort() {
        population.sort(new SolutionComparator());
        for (int i = population.size() - 1; i > POP_Size - 1; i--)
            population.remove(i);
    }

    // void getdistance() {
    //     for (int k = 0; k < N_OBJ; k++) {
    //         int h = k;
    //         Collections.sort(population, new Comparator<Solution>() {
    //             public int compare(Solution s1, Solution s2) {
    //                 if (s1.object[h] > s2.object[h])
    //                     return -1;
    //                 else if (s1.object[h] == s2.object[h])
    //                     return 0;
    //                 else
    //                     return 1;
    //             }
    //         });
    //         population.get(1).distance += 1000000;
    //         population.get(POP_Size - 1).distance += 1000000;
    //         for (int i = 1; i < POP_Size - 1; i++) {
    //             population.get(i).distance += -population.get(i + 1).object[k] + population.get(i - 1).object[k];
    //         }
    //     }
    // }
}